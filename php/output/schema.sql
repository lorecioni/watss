-- Exported WATSS database --

CREATE TABLE `avatars` (
  `peopleid` int(11) NOT NULL AUTO_INCREMENT,
  `face` int(11) NOT NULL,
  `face_z` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`peopleid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `cameras` (
  `cameraid` int(11) NOT NULL AUTO_INCREMENT,
  `calibration` int(11) NOT NULL,
  `calib_intrinsic` text,
  `calib_rotation` text,
  `calib_translation` text,
  `param` double DEFAULT '0.5',
  PRIMARY KEY (`cameraid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `poi` (
  `poiid` int(11) NOT NULL,
  `cameraid` int(11) NOT NULL,
  `location_x` int(11) NOT NULL,
  `location_y` int(11) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`poiid`,`cameraid`),
  KEY `cameraid` (`poiid`,`cameraid`),
  KEY `poi_fk_cameraid_idx` (`cameraid`),
  CONSTRAINT `poi_fk_cameraid` FOREIGN KEY (`cameraid`) REFERENCES `cameras` (`cameraid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `groups` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL,
  PRIMARY KEY (`groupid`,`userid`),
  KEY `userid` (`userid`),
  CONSTRAINT `group_fk_userid` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `frames` (
  `frameid` int(11) NOT NULL,
  `cameraid` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`frameid`,`cameraid`),
  KEY `frameid` (`frameid`,`cameraid`),
  KEY `frameid_2` (`frameid`),
  KEY `cameraid` (`cameraid`),
  CONSTRAINT `frames_fk_cameraid` FOREIGN KEY (`cameraid`) REFERENCES `cameras` (`cameraid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `people` (
  `peopleid` int(11) NOT NULL,
  `frameid` int(11) NOT NULL,
  `cameraid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `bb_x` int(11) NOT NULL,
  `bb_y` int(11) NOT NULL,
  `bb_width` int(11) NOT NULL,
  `bb_height` int(11) NOT NULL,
  `bbV_x` int(11) NOT NULL,
  `bbV_y` int(11) NOT NULL,
  `bbV_width` int(11) NOT NULL,
  `bbV_height` int(11) NOT NULL,
  `gazeAngle_face` int(3) NOT NULL,
  `gazeAngle_face_z` int(3) NOT NULL,
  `gazeAngle_body` int(3) NOT NULL,
  `gazeAngle_body_z` int(3) NOT NULL,
  `color` varchar(7) NOT NULL,
  `poiid` int(11) DEFAULT NULL,
  `groupid` int(11) DEFAULT NULL,
  PRIMARY KEY (`peopleid`,`frameid`,`cameraid`,`userid`),
  KEY `poiid` (`poiid`),
  KEY `peopleid` (`peopleid`),
  KEY `userid` (`userid`),
  KEY `cameraid` (`cameraid`),
  KEY `groupid` (`groupid`),
  KEY `frameid` (`frameid`),
  CONSTRAINT `people_fk_cameraid` FOREIGN KEY (`cameraid`) REFERENCES `cameras` (`cameraid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `people_fk_frameid` FOREIGN KEY (`frameid`) REFERENCES `frames` (`frameid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `people_fk_groupid` FOREIGN KEY (`groupid`) REFERENCES `groups` (`groupid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `people_fk_peopleid` FOREIGN KEY (`peopleid`) REFERENCES `avatars` (`peopleid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `people_fk_poi` FOREIGN KEY (`poiid`) REFERENCES `poi` (`poiid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `people_fk_userid` FOREIGN KEY (`userid`) REFERENCES `users` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

